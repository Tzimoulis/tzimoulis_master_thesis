**Athanasios Tzimoulis Master Thesis: Buildings Subjected to Earthquake - Comparative Studies**

*Munich 2014*



The folder contains four *.dat* files written in Sofistik 2013 CADINP language, implementing the workflow of the Eurocode 8 earthquake analysis methods in Sofistik.